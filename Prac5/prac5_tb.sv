//Define the ALU testbench module
module ALU_tb();    
//inputs
reg clk;
reg[7:0] A,B;
reg[3:0] ALU_Sel;
// output
wire [7:0] ALU_Out; 

//Instantiate the design under test
ALU dut(
    .clk(clk),
    .A(A),
    .B(B),
    .ALU_Sel(ALU_Sel),
    .ALU_out(ALU_Out));

initial begin //Initial means this only happens once
    $dumpfile("dump.vcd");
    $dumpvars;
  	//setup waveform 
  	$display("A  B  ALU_Sel  ALU_Out");
    $monitor("%b  %b  %b     %b",A,B,ALU_Sel, ALU_Out);
    
    clk = 1'b1;
    A = 8'b10110001;
    B = 8'b1;
    ALU_Sel = 4'b0;
    #5  //Note: This is not synthesizable and only available in simulation
        clk=!clk;
    #5
        clk=!clk;
        ALU_Sel = 4'b0000;
     //Call ADD operation#5
        clk=!clk;
    #5
        clk=!clk;
        ALU_Sel = 4'b0110;
     //Call MAC operation#5
        clk=!clk;
    #5
        clk=!clk;
        ALU_Sel = 4'b1000;
     //Call ROR operation#5
        clk = !clk;
    #5
        clk=!clk;
        ALU_Sel = 4'b1111;
  
  //Call LTH operation   
	end
endmodule
