//Define the module
module ALU(
    input clk, //Clock signal input
  input [7:0] A,B, //ALU 8 bit inputs
  input [3:0] ALU_Sel,	//ALU select input
  output reg [7:0] ALU_out //ALU 8 bit output
);

  reg [7:0] Acc; //Internal accumulator register
  assign ALU_Out = Acc;

always@(posedge clk) //Triggered on rising edge clock
    begin
        case(ALU_Sel)
          
          	4'b0000: //Addition
              Acc = A + B;
          
          	4'b0001: //Subtraction
              Acc = A - B;
          	
          	4'b0010: //Multiplication
              Acc = A * B;
          	
          	4'b0011: //Division
              Acc = A / B;
          	
          	4'b0100: //Add A to Acc value
              Acc = Acc + A;
          	
          	4'b0101: //Multiply Acc by A
              Acc = Acc * A;
          	
          	4'b0110: //Add A times B to Acc
              Acc = Acc + (A * B);
          	
          	4'b0111: //Logical shift left
              Acc = {A[6:0],A[7]};
          	
          	4'b1000: //Logical shift right
              Acc = {A[0],A[7:1]};
          	
          	4'b1001: //Bitwise AND
              Acc = A & B;
          	
          	4'b1010: //Bitwise OR 
              Acc = A | B;
          	
          	4'b1011: //Bitwise XOR
              Acc = A ^ B;
          	
          	4'b1100: //Bitwise NAND
              Acc = ~(A & B);
          	
          	4'b1101: //Acc = 0xFF is A=B else 0
              Acc = (A==B)?8'hFF:8'h0 ;
          	
          	4'b1110: //Acc = 0xFF is A>B else 0
              Acc = (A>B)?8'hFF:8'h0 ;
          	
          	4'b1111: //Acc = 0xFF is A<B else 0
              Acc = (A<B)?8'hFF:8'h0 ;
              
            default: Acc = A; //Default case
        endcase

        ALU_out = Acc;
    end    
endmodule
