import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import time
import threading
import RPi.GPIO as GPIO

delay_time = 10
time_of_sample = 0
def setup():
    GPIO.setup(22,GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(22,GPIO.FALLING, callback=change_time,bouncetime=200) #define button to change sample time of sensors
    
# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

# create the cs (chip select)
cs = digitalio.DigitalInOut(board.D5)

# create the mcp object
mcp = MCP.MCP3008(spi, cs)

# create an analog input channel on pin 3 for the light sensor and pin 1 for the temp sensor
chan = AnalogIn(mcp, MCP.P3)
temp_sensor = AnalogIn(mcp, MCP.P1)


def change_time(channel):   #function to change the sensor sampling time when button pressed
    global delay_time
    if delay_time==10:
        delay_time = 5
    elif delay_time ==5:
        delay_time = 1
    else:
        delay_time = 10


def get_temp_from_voltage(voltage): #return a temperature value from a voltage according to the temp sensor function
    temp = round((voltage - 0.5) / 0.01,2)
    return temp


def print_values_thread(): #create thread to print values
    global delay_time, time_of_sample
    thread = threading.Timer(delay_time, print_values_thread) #define thread

    thread.daemon = True  # Daemon threads exit when the program does
    thread.start()

    temp_in_degrees = get_temp_from_voltage(temp_sensor.voltage) #get temperature from conversion function
 
    print(str(time_of_sample)+'s\t\t' + str(temp_sensor.value) +'\t\t'+ str(temp_in_degrees) +' C\t\t' + str(chan.value))
    time_of_sample = time_of_sample + delay_time #increase time by sample time


if __name__ == "__main__": 
    setup()
    print("Runtime \tTemp Reading \tTemp \t\tLight Reading")
    print_values_thread() # call it once to start the thread
        
    # Tell our program to run indefinitely
    while True:
        pass
